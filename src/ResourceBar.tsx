const ResourceBar = ({
  amount,
  type
}: {
  /** The total amount of the stat */
  amount: number;
  /** The type of resource, must be either Health, Mana or Ferocity (white). Any other stat will result in a red bar. */
  type: string;
}): JSX.Element => (
  <div className="ResourceBarWrapper">
    <style jsx>{`
      display: block;
      height: 128px;
      width: 16px;
      .ResourceBarImage {
        background: ${(() => {
          switch (type) {
            case 'Health':
              return 'red';
            case 'MANA':
              return 'blue';
            case 'Ferocity':
              return 'white';
            default:
              return 'orange';
          }
        })()};
        background-position: right;
        width: 128px;
        height: 16px;
        transform: rotate(90deg) translateY(-16px);
        transform-origin: top left;
        text-align: center;
        position: absolute;
        color: #000;
        font-weight: 900;
        line-height: 16px;
      }
    `}</style>
    <div className="ResourceBarImage">{amount}</div>
  </div>
);

export default ResourceBar;
