import { createContext } from 'react';
import Ddragon from 'ddragon';

export const DdragonContext = createContext<Ddragon>(undefined);
